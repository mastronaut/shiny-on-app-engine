# get shiny serves plus tidyverse packages image
FROM rocker/shiny:latest

# system libraries of general use
RUN apt-get update && apt-get install -y \
    sudo \
    pandoc \
    pandoc-citeproc \
    libcurl4-gnutls-dev \
    libcairo2-dev \
    libxt-dev \
    libssl-dev \
    libssh2-1-dev   

# install R packages required 
# (change it dependeing on the packages you need)
RUN R -e "install.packages('shiny', repos='http://cran.rstudio.com/')"

# copy the app to the image
COPY app.R /srv/shiny-server/

# select port
EXPOSE 8080

# allow permission
RUN sudo chown -R shiny:shiny /srv/shiny-server

# run app
COPY shiny-server.sh /usr/bin/shiny-server.sh
# make script executable
RUN sudo chmod a+x /usr/bin/shiny-server.sh

# change listening port 
RUN sed -i 's/3838/8080/' /etc/shiny-server/shiny-server.conf

CMD ["/usr/bin/shiny-server.sh"]